/**
 * Unsusbscribe from newsletters
 *
 * Expects email address to be provided in request sting:
 * /unsubscribe?e=email@server.com
 */

import url from "url";
import { connect } from "./db";

function isValidEmail(email) {
  const emailRegex = /\S+@\S+/; // quite simple, causes many false positives
  return emailRegex.test(email.toLowerCase());
}

async function disableSubscriber(email) {
  const db = await connect();

  const collection = await db.collection("subscribers");

  // Don't actually delete but make subscriber inactive
  return await collection.findOneAndUpdate(
    { email },
    {
      $set: {
        active: false,
        update_ts: Date.now(),
      },
    },
    { upsert: true }
  );
}

export default async (req, resp) => {
  let responseBody = { error: 1 };

  const requestParams = url.parse(req.url, true).query;

  if (requestParams.e && isValidEmail(requestParams.e)) {
    await disableSubscriber(requestParams.e);

    responseBody = { success: 1 };
  }

  resp.setHeader("Content-Type", "application/json");
  resp.end(JSON.stringify(responseBody));
};
