/**
 * Subscribe to newsletters
 */
import url from "url";
import { connect } from "./db";

async function insertSubscriber({ email, ip }) {
  const db = await connect();

  const collection = await db.collection("subscribers");

  return await collection.findOneAndUpdate(
    { email, ip },
    {
      $set: {
        active: true,
        create_ts: Date.now(),
      },
    },
    { upsert: true }
  );
}

function isValidEmail(email) {
  const emailRegex = /\S+@\S+/; // quite simple, causes many false positives
  return emailRegex.test(email.toLowerCase());
}

export default async (req, response) => {
  let responseBody = { error: 1 };

  const requestParams = url.parse(req.url, true).query;

  // TODO: prevent multiple inserts from same IP?

  // TODO: error handling

  if (requestParams.e && isValidEmail(requestParams.e)) {
    await insertSubscriber({
      email: requestParams.e,
      ip: req.headers["x-real-ip"] || req.connection.remoteAddress,
    });

    responseBody = { success: 1 };
  }

  response.setHeader("Content-Type", "application/json");
  response.end(JSON.stringify(responseBody));
};
