const MongoClient = require("mongodb").MongoClient;
const { dbHost, dbUser, dbPassword, dbName } = require("./config");

const dbConnectionUrl = `mongodb+srv://${dbUser}:${dbPassword}@${dbHost}/${dbName}?retryWrites=true&w=majority`;
const dbConnectionOptions = { useNewUrlParser: true };

// Create cached connection variable
let cachedDb = null;

export async function connect() {
  // If the database connection is cached, use it instead of creating a new connection
  if (cachedDb) {
    return cachedDb;
  }

  // If no connection is cached, create a new one
  const client = await MongoClient.connect(
    dbConnectionUrl,
    dbConnectionOptions
  );

  const db = await client.db(dbName);

  // Cache the database connection and return the connection
  cachedDb = db;
  return db;
}
