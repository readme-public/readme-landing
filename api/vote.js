/**
 * Register vote
 */
import url from "url";
import { connect } from "./db";

async function insertVote(vote) {
  const db = await connect();

  const collection = await db.collection("votes");

  return await collection.insertOne(vote);
}

function isAllowedChoice(vote) {
  const choices = [1, 2, 3]; // only allow these values
  return choices.includes(parseInt(vote));
}

export default async (request, response) => {
  let responseBody = { error: 1 };

  const requestParams = url.parse(request.url, true).query;

  // TODO: prevent multiple votes from same IP?

  // TODO: error handling

  if (requestParams.v && isAllowedChoice(requestParams.v)) {
    await insertVote({
      choice: parseInt(requestParams.v),
      ip: request.headers["x-real-ip"] || req.connection.remoteAddress,
      ts: Date.now(),
    });

    responseBody = { success: 1 };
  }

  response.setHeader("Content-Type", "application/json");
  response.end(JSON.stringify(responseBody));
};
