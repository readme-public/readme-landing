import Head from "../components/head";
import { config } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import * as fa from "@fortawesome/free-solid-svg-icons";
import fetch from "isomorphic-unfetch";

config.autoAddCss = false;

// Unsubscribtion result status codes
const UNSUBSCRIBE_STATUS_OK = 1;
const UNSUBSCRIBE_STATUS_ERROR = 2;

const getBaseUrl = req =>
  req
    ? `${req.headers["x-forwarded-proto"]}://${req.headers["x-forwarded-host"]}`
    : "";

function isValidEmail(email) {
  const emailRegex = /\S+@\S+/; // quite simple, causes many false positives
  return emailRegex.test(email.toLowerCase());
}

async function unsubscribe(baseUrl, email) {
  const res = await fetch(`${baseUrl}/api/unsubscribe.js?e=${email}`);
  return await res.json();
}

const Unsubscribe = ({ status, email }) => (
  <>
    <Head title="Unsubscribe - Pressbe" />

    <header>
      <h1>Unsubscribe</h1>
      <FontAwesomeIcon icon={fa.faBell} size="4x" />
    </header>

    {status === UNSUBSCRIBE_STATUS_OK && (
      <main>
        <p>
          That's a pity that you're leaving... Anyways, we are always glad to
          see you back again!
        </p>
        <p>
          <strong>{email}</strong> has been successfully removed from our
          records.
        </p>
      </main>
    )}

    {status === UNSUBSCRIBE_STATUS_ERROR && (
      <main>Oops, something went wrong.</main>
    )}
  </>
);

Unsubscribe.getInitialProps = async ({ req, query: { e: email } }) => {
  const props = { status: UNSUBSCRIBE_STATUS_ERROR, email: null };

  if (email && isValidEmail(email)) {
    props.email = email;

    // try to unsubscribe
    try {
      const baseUrl = getBaseUrl(req);
      const json = await unsubscribe(baseUrl, email);

      if (json && json.success) {
        props.status = UNSUBSCRIBE_STATUS_OK;
      }
    } catch (e) {}
  }

  return props;
};

export default Unsubscribe;
