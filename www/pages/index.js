import { useState } from "react";
import Head from "../components/head";
import { config } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import * as fa from "@fortawesome/free-solid-svg-icons";
import Logo from "../public/logo.svg";
import Vote from "../components/vote";
import Subscribe from "../components/subscribe";
import mixpanel from "mixpanel-browser";
import Social from "../components/social";
import VoteLink from "../components/votelink";

// Turn off autoAddCSS feature, and load Fontawesome styles directly (imported in SASS),
// as autoloading occurs too late and the icons appear overscaled until CSS is loaded.
config.autoAddCss = false;

mixpanel.init("2590f792b7f6eb972304e0d8b94b9c4c");
mixpanel.track("Pressbe visited");

const Home = () => {
  const [voteLinkVisible, setVoteLinkVisible] = useState(true);
  const bodyClassModifier = "novotelink";
  // A delay before hiding completely the vote link, so that the fading out animation has time to complete.
  // The value is past 200ms of transition duration, which is defined in VoteLink component's CSS.
  const voteLinkVisibilityDelay = 300;

  const handleClose = () => {
    document.getElementsByTagName("body")[0].className += bodyClassModifier;
    setTimeout(() => setVoteLinkVisible(false), voteLinkVisibilityDelay);
  };

  return (
    <>
      {voteLinkVisible && <VoteLink onClose={handleClose} />}

      <Head
        title="Home — Pressbe"
        description="A place where everyone can contribute to the creation of a better reading experience."
      />

      <div id="fb-root" />
      <header className="hero">
        <section className="hero-content">
          <Logo className="logo" />
          <h1>Meet Pressbe</h1>
          <p>A place where everyone can contribute to create a better reading experience.</p>
          <p>
            If you enjoy reading magazines, if you are a writer, or have something you want to
            share, then you'll like us.
          </p>
        </section>
      </header>

      <main>
        <section>
          Welcome to Pressbe — a platform where you can find and contribute to alternative sources
          of information in the form of periodical editions.
        </section>

        <section>
          <h2>Why we do this</h2>

          <div className="about-section">
            <FontAwesomeIcon icon={fa.faBrain} size="4x" />
            <p>
              Quite often people get stuck on one favorite thing. Favorite coffee, favorite music
              band, favorite food, etc. This is also valid for places we are getting information
              from. We might have a favorite blog, a newspaper, or a magazine. Having it, we might
              not think there is much more about the topic we are interested in.{" "}
              <em>
                We want the platform to bring an opportunity of discovering new sources of
                information about various topics.
              </em>{" "}
              People might like or not like them, but they will know these topics exist.
            </p>
          </div>

          <div className="about-section">
            <FontAwesomeIcon icon={fa.faUsers} size="4x" />
            <p>
              While many great things were invented by single persons, most of the things were
              invented by groups of people. We believe creating content by a group of people is a
              great thing proven by success of periodic issues. We want to bring{" "}
              <em>a new experience of creating content in groups online</em>, where authors can
              collaborate with each other, find where they want to be published, etc. And, as a
              result, provide greater content for the end reader.
            </p>
          </div>

          <div className="about-section">
            <FontAwesomeIcon icon={fa.faBullhorn} size="4x" />
            <p>
              There are lots of people having different opinions and points of view, lots of
              thoughts to share. However, in modern structure of web it’s difficult to do that.
              There might be plenty of reasons — sometimes it’s hard to reach the end reader,
              sometimes you might not have enough to share something periodically (e.g. have an own
              blog). Despite of all possible reasons{" "}
              <em>we believe everyone should have a chance to be heard</em>.
            </p>
          </div>

          <div className="about-section">
            <FontAwesomeIcon icon={fa.faMobile} size="4x" />
            <p>
              Nowadays there are lots of content created online. You might think about blogs, news
              feeds, online magazines. However, printed editions are still popular, and there are
              multiple reasons why. Mainly it’s not about some single story/article. When readers
              have a printed media, they have access to many articles which cover topics they are
              interested in, to a community of people who share the same interests, the same idea,
              and want to share it with you.{" "}
              <em>We want to bring the experience of reading printed editions online</em> without
              the need to have your favorite printed editions physically with you.
            </p>
          </div>
        </section>

        <section>
          <h2>How it works</h2>

          <section>
            <h3>Have something to share?</h3>

            <ul className="features">
              <li className="feature">
                <FontAwesomeIcon icon={fa.faEdit} size="3x" />
                <p>Write what you want to share</p>
              </li>

              <li className="feature">
                <FontAwesomeIcon icon={fa.faSearch} size="3x" />
                <p>Find an edition where you want to publish your content</p>
              </li>

              <li className="feature">
                <FontAwesomeIcon icon={fa.faShareSquare} size="3x" />
                <p>Send a proposal to the editor</p>
              </li>

              <li className="feature">
                <FontAwesomeIcon icon={fa.faNewspaper} size="3x" />
                <p>Get published!</p>
              </li>
            </ul>
          </section>

          <section>
            <h3>Want to start running your periodical edition?</h3>

            <ul className="features">
              <li className="feature">
                <FontAwesomeIcon icon={fa.faNewspaper} size="3x" />
                <p>Create your edition</p>
              </li>

              <li className="feature">
                <FontAwesomeIcon icon={fa.faInbox} size="3x" />
                <p>Get articles from great authors</p>
              </li>

              <li className="feature">
                <FontAwesomeIcon icon={fa.faTasks} size="3x" />
                <p>Choose ones which fit most</p>
              </li>

              <li className="feature">
                <FontAwesomeIcon icon={fa.faBook} size="3x" />
                <p>Build and publish an issue</p>
              </li>
            </ul>
          </section>

          <section>
            <h3>Enjoy reading?</h3>

            <ul className="features">
              <li className="feature">
                <FontAwesomeIcon icon={fa.faThumbsUp} size="3x" />
                <p>Search for a topic you like</p>
              </li>

              <li className="feature">
                <FontAwesomeIcon icon={fa.faBookReader} size="3x" />
                <p>Simply enjoy reading!</p>
              </li>
            </ul>
          </section>
        </section>

        <section>
          <h2>What's next?</h2>
          <p>We are in an active development phase and would like to hear from you.</p>

          <section>
            <h3 className="icon-header">
              <FontAwesomeIcon icon={fa.faQuestionCircle} />
              Have something to ask or tell us?
            </h3>
            <p>
              Let us know! We appreciate any comment, question and of course criticism. Just drop a
              mail at{" "}
              <a
                className="mail"
                href="mailto:%63%6f%6e%74%61%63%74%40%70%72%65%73%73%62%65%2e%63%6f%6d"
              >
                contact@pressbe.com
              </a>
              . We would love to hear from you!
            </p>
          </section>

          <section id="vote">
            <h3 className="icon-header" id="votelabel">
              <FontAwesomeIcon icon={fa.faThumbsUp} />
              Let us know how it sounds so far with just a single click:
            </h3>
            <Vote />
          </section>

          <section>
            <h3 className="icon-header">
              <FontAwesomeIcon icon={fa.faEnvelope} /> Want us to keep you updated?
            </h3>
            <p>
              Join the list to stay tuned and receive all the news! We promise to keep your email
              address in secret. And of course it's possible to unsubscribe anytime.
            </p>
            <h3>I'd love to be updated:</h3>
            <Subscribe />
          </section>

          <section className="social-section">
            <h3 className="icon-header">
              <FontAwesomeIcon icon={fa.faShareAlt} />
              Connect with us in social networks
            </h3>
            <Social />
          </section>
        </section>
      </main>

      <footer>&copy; 2020 Pressbe</footer>
    </>
  );
};

export default Home;
