import Head from "../components/head";

const NotFound = () => (
  <>
    <Head title="Page is not found - Pressbe" />

    <header>
      <h1>Oops, this page does not exist</h1>
    </header>
  </>
);

export default NotFound;
