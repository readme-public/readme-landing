import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import * as fa from "@fortawesome/free-solid-svg-icons";

const Subscribe = () => {
  const [subscribedState, setSubscribedState] = useState("ready");
  const [email, setEmail] = useState(null);

  const handleChange = event => {
    setEmail(event.target.value);
  };

  const handleSubmit = event => {
    if (email) {
      event.target[event.target.length - 1].blur(); // remove focus from submit button

      setSubscribedState("processing");
      fetch(`/api/subscribe.js?e=${email}`).then(() =>
        setSubscribedState("done")
      );
    }

    event.preventDefault();
  };

  return (
    <div className={"form form--" + subscribedState}>
      <form onSubmit={handleSubmit}>
        <input
          type="email"
          required
          placeholder="Email address"
          onChange={handleChange}
        />
        <input type="submit" />
      </form>

      <h4
        className="form__submit-message form__submit-message--processing"
        role="alert"
        aria-live="assertive"
      >
        <FontAwesomeIcon icon={fa.faCircleNotch} className="fa-spin" /> Saving
        your subscription...
      </h4>

      <h3
        className="form__submit-message form__submit-message--done"
        role="alert"
        aria-live="assertive"
      >
        Thank you and welcome onboard, it will be exciting!
      </h3>
    </div>
  );
};

export default Subscribe;
