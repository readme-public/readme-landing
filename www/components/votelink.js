import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import * as fa from "@fortawesome/free-solid-svg-icons";
import "./votelink.scss";

const VoteLink = ({ onClose }) => {
  const [linkClassModifier, setLinkClassModifier] = useState("");

  const handleClick = () => {
    setLinkClassModifier("votelink--closing");
    onClose();
  };

  return (
    <div className={"votelink " + linkClassModifier}>
      <div className="votelink__wrapper">
        <a href="/#vote">
          <FontAwesomeIcon icon={fa.faThumbsUp} />
          <div className="votelink__label">Give us feedback with one click</div>
        </a>
        <button aria-label="Hide the banner" className="votelink__close" onClick={handleClick}>
          &times;
        </button>
      </div>
    </div>
  );
};

export default VoteLink;
