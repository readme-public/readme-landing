import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import * as fa from "@fortawesome/free-solid-svg-icons";
import "./vote.scss";

const Vote = () => {
  const [votedState, setVotedState] = useState("ready");
  const [choice, setChoice] = useState(0);

  const handleChoice = (event, choice) => {
    event.target.blur();
    setChoice(choice);
  };

  useEffect(() => {
    if (choice) {
      setVotedState("processing");
      fetch(`/api/vote.js?v=${choice}`).then(() => setVotedState("done"));
    }
  }, [choice]);

  return (
    <div className={"vote form form--" + votedState}>
      <ul aria-labelledby="votelabel">
        <li className="vote__choice">
          <button onClick={e => handleChoice(e, 1)}>
            <FontAwesomeIcon icon={fa.faSmile} /> Very promising!
          </button>
        </li>
        <li className="vote__choice">
          <button onClick={e => handleChoice(e, 2)}>
            <FontAwesomeIcon icon={fa.faMeh} /> Somewhat interesting
          </button>
        </li>
        <li className="vote__choice">
          <button onClick={e => handleChoice(e, 3)}>
            <FontAwesomeIcon icon={fa.faFrown} /> Boring and secondary
          </button>
        </li>
      </ul>

      <h4
        className="form__submit-message form__submit-message--processing"
        role="alert"
        aria-live="assertive"
      >
        <FontAwesomeIcon icon={fa.faCircleNotch} className="fa-spin" />{" "}
        Submitting the vote...
      </h4>

      <h3
        className="form__submit-message form__submit-message--done"
        role="alert"
        aria-live="assertive"
      >
        Thank you so much for sharing your opinion!
      </h3>
    </div>
  );
};

export default Vote;
