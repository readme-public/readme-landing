import React from "react";
import { FacebookProvider, Like, Group } from "react-facebook";

const Social = () => (
  <>
    <FacebookProvider appId="342140040049603">
      <Group
        href="https://www.facebook.com/groups/pressbe"
        width="300"
        showSocialContext={true}
        showMetaData={true}
        skin="light"
      />
    </FacebookProvider>
    <br />
    <FacebookProvider appId="342140040049603">
      <Like
        href="http://pressbe.com"
        colorScheme="dark"
        share
        showFaces={false}
        width={"300px"}
      />
    </FacebookProvider>
  </>
);

export default Social;
