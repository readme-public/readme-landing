const withCSS = require("@zeit/next-css");
const withSass = require("@zeit/next-sass");

const configObject = {
  target: "serverless", // enable serverless mode for build

  webpack(config, options) {
    config.module.rules.push({
      test: /\.svg$/,
      use: ["@svgr/webpack"], // allows importing svg files as React components
    });

    return config;
  },
};

// Consider "next-compose-plugins" module in case if this nested chain becomes too deep
module.exports = withCSS(withSass(configObject));
