# NPM commands

## Install, build, and run a local instance

All the commands below assume the current directory is _readme-landing_.

### Prerequisites

It works best way with Next.js "now" package installed globally:

```sh
npm i -g now
```

### Install packages

```sh
npm install
```

### Run local server

```sh
now dev
```

The project is then available at http://localhost:3000/.

### Staging package into the cloud (requires registered account)

```sh
now
```

### Deploy package to production

```sh
now --prod
```
